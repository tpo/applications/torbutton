<!--
   - Copyright (c) 2019, The Tor Project, Inc.
   - See LICENSE for licensing information.
   - vim: set sw=2 sts=2 ts=8 et syntax=xml:
  -->

<!ENTITY aboutTor.title "Tori buruz">

<!ENTITY aboutTor.viewChangelog.label "Ikusi Aldaketak">

<!ENTITY aboutTor.ready.label "Esploratu. Pribatuki.">
<!ENTITY aboutTor.ready2.label "Prest zaude munduko nabigazio esperientzia pribatuenerako.">
<!ENTITY aboutTor.failure.label "Zerbait gaizki joan da!">
<!ENTITY aboutTor.failure2.label "Tor ez da nabigatzaile honetan funtzionatzen ari.">

<!ENTITY aboutTor.search.label "Bilatu DuckDuckGo erabiliz">
<!ENTITY aboutTor.searchDDGPost.link "https://duckduckgo.com">

<!ENTITY aboutTor.torbrowser_user_manual_questions.label "Galderarik?">
<!ENTITY aboutTor.torbrowser_user_manual_link.label "Aztertu gure Tor Browseren eskuliburua">
<!-- The next two entities are used within the browser's Help menu. -->
<!ENTITY aboutTor.torbrowser_user_manual.accesskey "M">
<!ENTITY aboutTor.torbrowser_user_manual.label "Tor nabigatzailearen eskuliburua">

<!ENTITY aboutTor.tor_mission.label "The Tor Project irabazi asmorik gabeko US 501(c)(3) erakunde bat da, anonimotasun eta pribatutasun teknologia libre eta kode irekikoak sortuz eta zabalduz giza eskubideak eta askatasunak sustatzen dituena, haien murrizketarik gabeko eskuragarritasuna eta erabilera babestuz, eta haien ulertze zientifiko eta herrikoia sustatuz.">
<!ENTITY aboutTor.getInvolved.label "Hartu parte »">

<!ENTITY aboutTor.newsletter.tagline "Jaso Toren inguruko azken berriak zure sarrerako ontzian.">
<!ENTITY aboutTor.newsletter.link_text "Harpidetu Tor berrietara">
<!ENTITY aboutTor.donationBanner.freeToUse "Tor is free to use because of donations from people like you.">
<!ENTITY aboutTor.donationBanner.buttonA "Egin dohaintza orain">

<!ENTITY aboutTor.alpha.ready.label "Test. Thoroughly.">
<!ENTITY aboutTor.alpha.ready2.label "You’re ready to test the world’s most private browsing experience.">
<!ENTITY aboutTor.alpha.bannerDescription "Tor Browser Alpha is an unstable version of Tor Browser you can use to preview new features, test their performance and provide feedback before release.">
<!ENTITY aboutTor.alpha.bannerLink "Report a bug on the Tor Forum">

<!ENTITY aboutTor.nightly.ready.label "Test. Thoroughly.">
<!ENTITY aboutTor.nightly.ready2.label "You’re ready to test the world’s most private browsing experience.">
<!ENTITY aboutTor.nightly.bannerDescription "Tor Browser Nightly is an unstable version of Tor Browser you can use to preview new features, test their performance and provide feedback before release.">
<!ENTITY aboutTor.nightly.bannerLink "Report a bug on the Tor Forum">

<!-- YEC 2022 campaign https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41303 -->
<!-- LOCALIZATION NOTE (aboutTor.yec2022.powered_by_privacy): a header for a list of things which are powered by/enabled by/possible due to privacy (each item should have positive connotations/associations in the translated languages) -->
<!ENTITY aboutTor.yec2022.powered_by_privacy "POWERED BY PRIVACY:">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.resistance): resistance as in social/political resistance to opression, injustice, etc -->
<!ENTITY aboutTor.yec2022.resistance "RESISTANCE">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.change): change as in the process of social/political progress toward a better/more equitable society -->
<!ENTITY aboutTor.yec2022.change "CHANGE">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.freedom): freedom as in liberty, protection against exploition, imprisonment, etc -->
<!ENTITY aboutTor.yec2022.freedom "FREEDOM">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.donate_now): Label for a button directing user to donation page-->
<!ENTITY aboutTor.yec2022.donate_now "DONATU ORAIN">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.donation_matching): Please translate the 'Friends of Tor' phrase, but
also format it like the name of an organization in whichever way that is appropriate for your locale.

Please keep the currency in USD.
Thank you!
-->
<!ENTITY aboutTor.yec2022.donation_matching "Tor-en lagunak taldeak berdinduko du zure donazioa, $100,000 arte.">
