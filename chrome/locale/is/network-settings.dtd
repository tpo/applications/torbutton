<!ENTITY torsettings.dialog.title "Netkerfisstillingar Tor">
<!ENTITY torsettings.wizard.title.default "Tengjast við Tor-netið">
<!ENTITY torsettings.wizard.title.configure "Netkerfisstillingar Tor">
<!ENTITY torsettings.wizard.title.connecting "Kem á tengingu">

<!-- For locale picker: -->
<!ENTITY torlauncher.localePicker.title "Tungumál Tor-vafra">
<!ENTITY torlauncher.localePicker.prompt "Veldu tungumál">

<!-- For "first run" wizard: -->

<!ENTITY torSettings.connectPrompt "Smelltu á &quot;Tengjast&quot; til að tengjast við Tor.">
<!ENTITY torSettings.configurePrompt "Smelltu á “Stilla” til að breyta netstillingum ef þú ert í landi sem ritskoðar umferð um Tor (svo sem Egyptalandi, Kína, Tyrklandi) eða ef þú ert að tengjast af einkaneti sem krefst þess að fara um milliþjón.">
<!ENTITY torSettings.configure "Stilla">
<!ENTITY torSettings.connect "Tengjast">

<!-- Other: -->

<!ENTITY torsettings.startingTor "Bíð eftir að Tor ræsist...">
<!ENTITY torsettings.restartTor "Endurræsa Tor">
<!ENTITY torsettings.reconfigTor "Endurstilla">

<!ENTITY torsettings.discardSettings.prompt "Þú hefur sett upp brýr fyrir Tor eða að þú hefur farið inn í stillingar milliþjóns á kerfinu.&#160; Til að útbúa beina tengingu inn á Tor-netið, þá verður að fjarlægja þessar stillingar.">
<!ENTITY torsettings.discardSettings.proceed "Fjarlægja stillingar og tengjast">

<!ENTITY torsettings.optional "Valkvætt">

<!ENTITY torsettings.useProxy.checkbox "Ég nota milliþjón (proxy) til að tengjast við internetið">
<!ENTITY torsettings.useProxy.type "Tegund milliþjóns (proxy)">
<!ENTITY torsettings.useProxy.type.placeholder "veldu tegund milliþjóns">
<!ENTITY torsettings.useProxy.address "Vistfang">
<!ENTITY torsettings.useProxy.address.placeholder "IP-vistfang eða vélarheiti">
<!ENTITY torsettings.useProxy.port "Gátt">
<!ENTITY torsettings.useProxy.username "Notandanafn">
<!ENTITY torsettings.useProxy.password "Lykilorð">
<!ENTITY torsettings.useProxy.type.socks4 "SOCKS 4">
<!ENTITY torsettings.useProxy.type.socks5 "SOCKS 5">
<!ENTITY torsettings.useProxy.type.http "HTTP / HTTPS">
<!ENTITY torsettings.firewall.checkbox "Þessi tölva tengist í gegnum eldvegg sem leyfir einungis tengingar í gegnum tilteknar gáttir">
<!ENTITY torsettings.firewall.allowedPorts "Leyfðar gáttir">
<!ENTITY torsettings.useBridges.checkbox "Tor er ritskoðað í landinu mínu">
<!ENTITY torsettings.useBridges.default "Veldu innbyggða brú">
<!ENTITY torsettings.useBridges.default.placeholder "veldu brú">
<!ENTITY torsettings.useBridges.bridgeDB "Biðja um brú frá torproject.org">
<!ENTITY torsettings.useBridges.captchaSolution.placeholder "Settu inn stafina úr myndinni">
<!ENTITY torsettings.useBridges.reloadCaptcha.tooltip "Fá nýja gátu">
<!ENTITY torsettings.useBridges.captchaSubmit "Senda inn">
<!ENTITY torsettings.useBridges.custom "Gefa brú sem ég þekki">
<!ENTITY torsettings.useBridges.label "Settu inn upplýsingar um brú frá aðila sem þú treystir.">
<!ENTITY torsettings.useBridges.placeholder "settu inn vistfang:gátt (eitt á hverja línu)">

<!ENTITY torsettings.copyLog "Afrita atvikaskrá Tor á klippispjald">

<!ENTITY torsettings.proxyHelpTitle "Hjálp fyrir milliþjóna">
<!ENTITY torsettings.proxyHelp1 "Hugsanlega þarf að nota staðværan milliþjón (proxy) þegar tengst er í gegnum net fyrirtækis, skóla eða stofnunar.&#160;Ef þú ert ekki viss hvort þurfi að nota milliþjón, kíktu þá á netstillingarnar í einhverjum öðrum vafra eða á uppsetningu netsins í stýrikerfinu þínu.">

<!ENTITY torsettings.bridgeHelpTitle "Hjálp fyrir brúaendurvarpa">
<!ENTITY torsettings.bridgeHelp1 "Brýr eru faldir endurvarpar sem gera erfiðara að loka á tengingar á Tor netinu.&#160; Hver tegund af brúm notar mismunandi aðferðir til að forðast ritskoðun.&#160; obfs-brýrnar láta netumferðina þína líta út sem handahófskennt suð og meek-brýr láta netumferðina þína líta út fyrir að tengjast þeirri þjónustu í staðinn fyrir Tor.">
<!ENTITY torsettings.bridgeHelp2 "Vegna þess hvernig sum lönd reyna að loka á umferð um Tor, munu sumar brýr virka í sumum löndum en aðrar ekki.&#160; Ef þú ert ekki viss um hvaða brýr virka í landinu þínu, skaltu skoða torproject.org/about/contact.html#support">

<!-- Progress -->
<!ENTITY torprogress.pleaseWait "Bíddu aðeins á meðan tengingu er komið á við Tor-netið.&#160; Það getur tekið nokkrar mínútur.">

<!-- #31286 about:preferences strings -->
<!ENTITY torPreferences.categoryTitle "Tenging">
<!ENTITY torPreferences.torSettings "Stillingar Tor">
<!ENTITY torPreferences.torSettingsDescription "Tor-vafrinn beinir umferðinni þinni um Tor-netið, sem rekið er af þúsundum sjálfboðaliða um víða veröld." >
<!ENTITY torPreferences.learnMore "Fræðast frekar">
<!-- Status -->
<!ENTITY torPreferences.statusInternetLabel "Internet:">
<!ENTITY torPreferences.statusInternetTest "Prófun">
<!ENTITY torPreferences.statusInternetOnline "Nettengt">
<!ENTITY torPreferences.statusInternetOffline "Ónettengt">
<!ENTITY torPreferences.statusTorLabel "Tor-netið:">
<!ENTITY torPreferences.statusTorConnected "Tengt">
<!ENTITY torPreferences.statusTorNotConnected "Ótengt">
<!ENTITY torPreferences.statusTorBlocked "Mögulega hindrað">
<!ENTITY torPreferences.learnMore "Fræðast frekar">
<!-- Quickstart -->
<!ENTITY torPreferences.quickstart "Flýtiræsing">
<!ENTITY torPreferences.quickstartDescriptionLong "Flýtiræsing tengir Tor-vafrann sjálfvirkt við Tor-netkerfið þegar hann er ræstur, út frá þeim stillingum sem síðast voru notaðar til að tengjast.">
<!ENTITY torPreferences.quickstartCheckbox "Alltaf tengjast sjálfkrafa">
<!-- Bridge settings -->
<!ENTITY torPreferences.bridges "Brýr">
<!ENTITY torPreferences.bridgesDescription "Brýr hjálpa þér við að fá aðgang að Tor-netkerfinu á stöðum þar sem lokað er á Tor. Það fer eftir því hvar þú ert hvaða brú virkar betur en einhver önnur.">
<!ENTITY torPreferences.bridgeLocation "Staðsetning þín">
<!ENTITY torPreferences.bridgeLocationAutomatic "Sjálfvirkt">
<!ENTITY torPreferences.bridgeLocationFrequent "Oft notaðar staðsetningar">
<!ENTITY torPreferences.bridgeLocationOther "Aðrar staðsetningar">
<!ENTITY torPreferences.bridgeChooseForMe "Velja brú fyrir mig…">
<!ENTITY torPreferences.bridgeBadgeCurrent "Fyrirliggjandi brýrnar þínar">
<!ENTITY torPreferences.bridgeBadgeCurrentDescription "Þú getur vistað eina eða fleiri brýr og Tor mun velja hverja þeirra ætti að nota þegar þú tengist. Tor mun skipta sjálfvirkt yfir á aðra brú eftir þörfum.">
<!ENTITY torPreferences.bridgeId "#1 brú: #2"> <!-- #1 = bridge type; #2 = bridge emoji id -->
<!ENTITY torPreferences.remove "Fjarlægja">
<!ENTITY torPreferences.bridgeDisableBuiltIn "Gera innbyggðar brýr óvirkar">
<!ENTITY torPreferences.bridgeShare "Deildu þessari brú með því að nota QR-kóðann eða að líma vistfang hennar:">
<!ENTITY torPreferences.bridgeCopy "Afrita vistfang brúar">
<!ENTITY torPreferences.copied "Afritað!">
<!ENTITY torPreferences.bridgeShowAll "Sýna allar brýr">
<!ENTITY torPreferences.bridgeRemoveAll "Fjarlægja allar brýr">
<!ENTITY torPreferences.bridgeAdd "Þæta við nýrri brú">
<!ENTITY torPreferences.bridgeSelectBrowserBuiltin "Veldu eina af innbyggðum brúm Tor-vafrans">
<!ENTITY torPreferences.bridgeSelectBuiltin "Velja innbyggða brú…">
<!ENTITY torPreferences.bridgeRequest "Biðja um brú…">
<!ENTITY torPreferences.bridgeEnterKnown "Settu inn vistfang brúar sem þú þekkir nú þegar">
<!ENTITY torPreferences.bridgeAddManually "Bæta handvirkt við brú…">
<!-- Advanced settings -->
<!ENTITY torPreferences.advanced "Nánar">
<!ENTITY torPreferences.advancedDescription "Stilltu hvernig Tor-vafrinn tengist við internetið">
<!ENTITY torPreferences.advancedButton "Stillingar…">
<!ENTITY torPreferences.viewTorLogs "Skoða atvikaskrár Tor">
<!ENTITY torPreferences.viewLogs "Skoða atvikaskrár…">
<!-- Remove all bridges dialog -->
<!ENTITY torPreferences.removeBridgesQuestion "Fjarlægja allar brýrnar?">
<!ENTITY torPreferences.removeBridgesWarning "Þessa aðgerð er ekki hægt að afturkalla.">
<!ENTITY torPreferences.cancel "Hætta við">
<!-- Scan bridge QR dialog -->
<!ENTITY torPreferences.scanQrTitle "Skannaðu QR-kóðann">
<!-- Builtin bridges dialog -->
<!ENTITY torPreferences.builtinBridgeTitle "Innbyggðar brýr">
<!ENTITY torPreferences.builtinBridgeDescription "Tor-vafrinn kemur með sérstakar tegundir brúa sem kallast 'Pluggable Transport' tengileiðir”.">
<!ENTITY torPreferences.builtinBridgeObfs4 "obfs4">
<!ENTITY torPreferences.builtinBridgeObfs4Description "obfs4 er tegund af innbyggðum brúm sem láta umferð um Tor líta út sem tilviljanakennda. Þær eru mun ólíklegri til að verða hindraðar heldur en forverarnir, obfs3-brýr.">
<!ENTITY torPreferences.builtinBridgeSnowflake "Snowflake">
<!ENTITY torPreferences.builtinBridgeSnowflakeDescription "Snowflake er innbyggð brú sem kemst framhjá ritskoðun með því að beina umferðinni þinni í gegnum Snowflake-milliþjóna, sem reknir eru af sjálfboðaliðum.">
<!ENTITY torPreferences.builtinBridgeMeekAzure "meek-azure">
<!ENTITY torPreferences.builtinBridgeMeekAzureDescription "Meek-azure er innbyggð brú sem lætur líta út eins og þú sért að nota Microsoft vefsvæði fremur en Tor.">
<!-- Request bridges dialog -->
<!ENTITY torPreferences.requestBridgeDialogTitle "Biðja um brú">
<!ENTITY torPreferences.requestBridgeDialogWaitPrompt "Tengist BridgeDB. Bíddu aðeins.">
<!ENTITY torPreferences.requestBridgeDialogSolvePrompt "Leystu CAPTCHA-þrautina til að biðja um brú.">
<!ENTITY torPreferences.requestBridgeErrorBadSolution "Þessi lausn er ekki rétt. Reyndu aftur.">
<!-- Provide bridge dialog -->
<!ENTITY torPreferences.provideBridgeTitle "Útvega brú">
<!ENTITY torPreferences.provideBridgeHeader "Settu inn upplýsingar um brúna sem þú fékkst frá áreiðanlegum aðila">
<!-- Connection settings dialog -->
<!ENTITY torPreferences.connectionSettingsDialogTitle "Tengistillingar">
<!ENTITY torPreferences.connectionSettingsDialogHeader "Stjórnaðu hvernig Tor-vafrinn tengist internetinu">
<!ENTITY torPreferences.firewallPortsPlaceholder "Gildi aðgreind með kommum">
<!-- Log dialog -->
<!ENTITY torPreferences.torLogsDialogTitle "Tor atvikaskrá (log)">

<!-- #24746 about:torconnect strings -->
<!ENTITY torConnect.notConnectedConcise "Ótengt">
<!ENTITY torConnect.connectingConcise "Tengist…">
<!ENTITY torConnect.tryingAgain "Prófa aftur…">
<!ENTITY torConnect.noInternet "Tor-vafrinn náði ekki að tengjast internetinu">
<!ENTITY torConnect.noInternetDescription "Þetta gæti frekar verið vandamál með tengingu fremur en að lokað sé á Tor. Athugaðu internettenginguna þína og stillingar á milliþjóni og eldvegg áður en þú reynir aftur.">
<!ENTITY torConnect.couldNotConnect "Tor-vafrinn náði ekki að tengjast Tor">
<!ENTITY torConnect.assistDescriptionConfigure "stillt tenginguna þína"> <!-- used as a text to insert as a link on several strings (#1) -->
<!ENTITY torConnect.assistDescription "Ef lokað er á Tor þar sem þú ert, gæti hjálpað að prófa að nota brú. Tengiaðstoðin getur valið eina slíka fyrir þig út frá staðsetningunni þinni, eða að þú getur í staðinn #1 handvirkt."> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.tryingBridge "Prófa brú…">
<!ENTITY torConnect.tryingBridgeAgain "Prófa einu sinni í viðbót…">
<!ENTITY torConnect.errorLocation "Tor-vafrinn náði ekki að staðsetja þig">
<!ENTITY torConnect.errorLocationDescription "Tor Browser needs to know your location in order to choose the right bridge for you. If you’d rather not share your location, #1 manually instead."> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.isLocationCorrect "Eru þessar staðsetningarstillingar réttar?">
<!ENTITY torConnect.isLocationCorrectDescription "Tor-vafrinn náði enn ekki að tengjast við Tor. Athugaðu vandlega stillingar á staðsetningu og reyndu aftur, eða farðu í að #1."> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.finalError "Tor-vafrinn náði enn ekki að tengjast">
<!ENTITY torConnect.finalErrorDescription "Despite its best efforts, connection assist was not able to connect to Tor. Try troubleshooting your connection and adding a bridge manually instead.">
<!ENTITY torConnect.breadcrumbAssist "Tengiaðstoð">
<!ENTITY torConnect.breadcrumbLocation "Staðsetningarstillingar">
<!ENTITY torConnect.breadcrumbTryBridge "Prófa brú">
<!ENTITY torConnect.automatic "Sjálfvirkt">
<!ENTITY torConnect.selectCountryRegion "Veldu land eða svæði">
<!ENTITY torConnect.frequentLocations "Oft notaðar staðsetningar">
<!ENTITY torConnect.otherLocations "Aðrar staðsetningar">
<!ENTITY torConnect.restartTorBrowser "Endurræsa Tor-vafrann">
<!ENTITY torConnect.configureConnection "Stilla tengingu…">
<!ENTITY torConnect.viewLog "Skoða atvikaskrár">
<!ENTITY torConnect.tryAgain "Reyndu aftur">
<!ENTITY torConnect.offline "Ekki næst í internetið">
<!ENTITY torConnect.connectMessage "Changes to Tor Settings will not take effect until you connect">
<!ENTITY torConnect.tryAgainMessage "Tor-vafranum mistókst að koma á tengingu við Tor-netið">
<!ENTITY torConnect.yourLocation "Staðsetning þín">
<!ENTITY torConnect.tryBridge "Prófa brú">
<!ENTITY torConnect.autoBootstrappingFailed "Sjálfvirk stilling mistókst">
<!ENTITY torConnect.autoBootstrappingFailed "Sjálfvirk stilling mistókst">
<!ENTITY torConnect.cannotDetermineCountry "Tókst ekki að ákvarða land notandans">
<!ENTITY torConnect.noSettingsForCountry "Engar stillingar fáanlegar fyrir staðsetninguna þína">
