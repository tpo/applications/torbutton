<!ENTITY torsettings.dialog.title "Tor ネットワーク設定">
<!ENTITY torsettings.wizard.title.default "Tor に接続する">
<!ENTITY torsettings.wizard.title.configure "Tor ネットワーク設定">
<!ENTITY torsettings.wizard.title.connecting "接続を確立しています">

<!-- For locale picker: -->
<!ENTITY torlauncher.localePicker.title "Tor Browser 言語">
<!ENTITY torlauncher.localePicker.prompt "言語を選択してください。">

<!-- For "first run" wizard: -->

<!ENTITY torSettings.connectPrompt "“接続” をクリックすると Tor に接続します。">
<!ENTITY torSettings.configurePrompt "Tor による通信を検閲する国（エジプト、中国、トルコ等）にいる場合やプロキシを要求するプライベートネットワークから接続する場合、「設定」をクリックしてネットワーク設定を調整します。">
<!ENTITY torSettings.configure "設定">
<!ENTITY torSettings.connect "接続">

<!-- Other: -->

<!ENTITY torsettings.startingTor "Tor の起動を待機中…">
<!ENTITY torsettings.restartTor "Tor を再起動">
<!ENTITY torsettings.reconfigTor "再設定">

<!ENTITY torsettings.discardSettings.prompt "Tor ブリッジが設定されるか、ローカルプロキシ設定が入力されるかしました。&#160; Tor ネットワークに直接接続するためには、これらの設定は削除する必要があります。">
<!ENTITY torsettings.discardSettings.proceed "設定と接続を削除">

<!ENTITY torsettings.optional "オプション">

<!ENTITY torsettings.useProxy.checkbox "インターネット接続にプロキシを使用する">
<!ENTITY torsettings.useProxy.type "プロキシの種類">
<!ENTITY torsettings.useProxy.type.placeholder "プロキシの種類を選択">
<!ENTITY torsettings.useProxy.address "アドレス">
<!ENTITY torsettings.useProxy.address.placeholder "IP アドレスまたはホスト名">
<!ENTITY torsettings.useProxy.port "ポート">
<!ENTITY torsettings.useProxy.username "ユーザー名">
<!ENTITY torsettings.useProxy.password "パスワード">
<!ENTITY torsettings.useProxy.type.socks4 "SOCKS 4">
<!ENTITY torsettings.useProxy.type.socks5 "SOCKS 5">
<!ENTITY torsettings.useProxy.type.http "HTTP / HTTPS">
<!ENTITY torsettings.firewall.checkbox "このコンピューターは特定のポートへの接続のみ許可するファイアーウォールを通します。">
<!ENTITY torsettings.firewall.allowedPorts "許可されたポート">
<!ENTITY torsettings.useBridges.checkbox "Tor は私の国では検閲されています">
<!ENTITY torsettings.useBridges.default "内臓ブリッジを選択">
<!ENTITY torsettings.useBridges.default.placeholder "ブリッジを選択">
<!ENTITY torsettings.useBridges.bridgeDB "torproject.org にブリッジをリクエストする">
<!ENTITY torsettings.useBridges.captchaSolution.placeholder "画像の文字を入力してください">
<!ENTITY torsettings.useBridges.reloadCaptcha.tooltip "チャレンジを更新する">
<!ENTITY torsettings.useBridges.captchaSubmit "送信">
<!ENTITY torsettings.useBridges.custom "あなたが知っているブリッジを入力">
<!ENTITY torsettings.useBridges.label "信頼できる情報源から入手したブリッジ情報を入力してください。">
<!ENTITY torsettings.useBridges.placeholder "“アドレス:ポート” の形式で入力 (1 行ずつ)">

<!ENTITY torsettings.copyLog "Tor ログをクリップボードにコピー">

<!ENTITY torsettings.proxyHelpTitle "プロキシヘルプ">
<!ENTITY torsettings.proxyHelp1 "会社、学校、大学等のネットワークを通して接続する場合、ローカルプロキシが必要になる場合があります。&#160;プロキシが必要であるかどうかわからない場合は、他のブラウザのインターネット設定を見るか、システムのネットワーク設定を確認してください。">

<!ENTITY torsettings.bridgeHelpTitle "ブリッジリレーのヘルプ">
<!ENTITY torsettings.bridgeHelp1 "ブリッジとは Tor ネットワークへの接続を検閲することを困難するための非公開のリレーです。&#160; 各種類のブリッジは検閲を避けるための異なる手法を利用しています。&#160; obfs はあなたのトラフィックをランダムなノイズのように見せかけ，meek はあなたのトラフィックを Tor ではない特定のサービスへの接続であるように見せかけます。">
<!ENTITY torsettings.bridgeHelp2 "特定の国が Tor を検閲するしようと試みている場合、それがどのような手法で行われているかによって、あるブリッジがある国では機能しても他の国では動かないという場合があります。&#160; もしどのブリッジがあなたの国で機能するかわからない場合は  torproject.org/about/contact.html#support にアクセスしてください。">

<!-- Progress -->
<!ENTITY torprogress.pleaseWait "Tor ネットワークへの接続が確立されるまでお待ちください。&#160; これには数分間かかることがあります。">

<!-- #31286 about:preferences strings -->
<!ENTITY torPreferences.categoryTitle "接続">
<!ENTITY torPreferences.torSettings "Tor の設定">
<!ENTITY torPreferences.torSettingsDescription "Tor Browser は通信トラフィックを Tor ネットワークを経由させて送信します。Tor ネットワークは世界中の何千ものボランティアによって運用されています。" >
<!ENTITY torPreferences.learnMore "詳細情報">
<!-- Status -->
<!ENTITY torPreferences.statusInternetLabel "インターネット:">
<!ENTITY torPreferences.statusInternetTest "テスト">
<!ENTITY torPreferences.statusInternetOnline "オンライン">
<!ENTITY torPreferences.statusInternetOffline "オフライン">
<!ENTITY torPreferences.statusTorLabel "Tor ネットワーク:">
<!ENTITY torPreferences.statusTorConnected "接続済み">
<!ENTITY torPreferences.statusTorNotConnected "接続していません">
<!ENTITY torPreferences.statusTorBlocked "ブロックされた可能性">
<!ENTITY torPreferences.learnMore "詳細情報">
<!-- Quickstart -->
<!ENTITY torPreferences.quickstart "クイックスタート">
<!ENTITY torPreferences.quickstartDescriptionLong "クイックスタートは、最後に使用した接続設定に基づいて、起動時に Tor Browser を Tor ネットワークに自動的に接続します。">
<!ENTITY torPreferences.quickstartCheckbox "常に自動的に接続">
<!-- Bridge settings -->
<!ENTITY torPreferences.bridges "ブリッジ">
<!ENTITY torPreferences.bridgesDescription "Bridge は Tor がブロックされる地域から Tor ネットワークにアクセスすることを手助けします。地域によって、ある Bridge が他のものよりうまく動作する可能性があります。">
<!ENTITY torPreferences.bridgeLocation "あなたの現在地">
<!ENTITY torPreferences.bridgeLocationAutomatic "自動">
<!ENTITY torPreferences.bridgeLocationFrequent "よく選択される現在地">
<!ENTITY torPreferences.bridgeLocationOther "その他の現在地">
<!ENTITY torPreferences.bridgeChooseForMe "最適なブリッジを選択…">
<!ENTITY torPreferences.bridgeBadgeCurrent "現在のブリッジ">
<!ENTITY torPreferences.bridgeBadgeCurrentDescription "ブリッジを複数保存することもでき、その場合 Tor は接続時にどれを使用するか選択します。Tor は必要に応じて自動的に別のブリッジに切り替えます。">
<!ENTITY torPreferences.bridgeId "#1 ブリッジ: #2"> <!-- #1 = bridge type; #2 = bridge emoji id -->
<!ENTITY torPreferences.remove "削除">
<!ENTITY torPreferences.bridgeDisableBuiltIn "内蔵ブリッジを無効化">
<!ENTITY torPreferences.bridgeShare "QR コードやブリッジアドレスでこのブリッジを共有できます:">
<!ENTITY torPreferences.bridgeCopy "ブリッジアドレスをコピー">
<!ENTITY torPreferences.copied "コピーされました">
<!ENTITY torPreferences.bridgeShowAll "すべてのブリッジを表示">
<!ENTITY torPreferences.bridgeRemoveAll "すべてのブリッジを削除">
<!ENTITY torPreferences.bridgeAdd "新しいブリッジを追加">
<!ENTITY torPreferences.bridgeSelectBrowserBuiltin "Tor Browser の内蔵ブリッジから選択してください">
<!ENTITY torPreferences.bridgeSelectBuiltin "内蔵ブリッジを選択…">
<!ENTITY torPreferences.bridgeRequest "ブリッジをリクエスト…">
<!ENTITY torPreferences.bridgeEnterKnown "あなたが知っているブリッジアドレスを入力する">
<!ENTITY torPreferences.bridgeAddManually "手動でブリッジを追加…">
<!-- Advanced settings -->
<!ENTITY torPreferences.advanced "詳細">
<!ENTITY torPreferences.advancedDescription "Tor Browser がインターネットに接続する方法を設定します">
<!ENTITY torPreferences.advancedButton "設定…">
<!ENTITY torPreferences.viewTorLogs "Tor ログを表示">
<!ENTITY torPreferences.viewLogs "ログを表示…">
<!-- Remove all bridges dialog -->
<!ENTITY torPreferences.removeBridgesQuestion "すべてのブリッジを削除しますか？">
<!ENTITY torPreferences.removeBridgesWarning "この操作は元に戻すことはできません。">
<!ENTITY torPreferences.cancel "キャンセル">
<!-- Scan bridge QR dialog -->
<!ENTITY torPreferences.scanQrTitle "QR コードをスキャン">
<!-- Builtin bridges dialog -->
<!ENTITY torPreferences.builtinBridgeTitle "内蔵ブリッジ">
<!ENTITY torPreferences.builtinBridgeDescription "Tor Browser は「pluggable transports」と呼ばれるいくつかの種類のブリッジを内蔵しています。">
<!ENTITY torPreferences.builtinBridgeObfs4 "obfs4">
<!ENTITY torPreferences.builtinBridgeObfs4Description "obfs4 は内蔵ブリッジの一種で、Tor トラフィックをランダムなものに見せかけます。前身の obfs3 ブリッジよりもブロックされにくくなっています。">
<!ENTITY torPreferences.builtinBridgeSnowflake "Snowflake">
<!ENTITY torPreferences.builtinBridgeSnowflakeDescription "Snowflake は内蔵ブリッジの一種で、ボランティアによって運営されている Snowflake プロキシを経由して接続することで検閲に対抗します。">
<!ENTITY torPreferences.builtinBridgeMeekAzure "meek-azure">
<!ENTITY torPreferences.builtinBridgeMeekAzureDescription "meek-azure は内蔵ブリッジの一種で、Tor ではなく Microsoft のウェブサイトを使用しているように見せかけます。">
<!-- Request bridges dialog -->
<!ENTITY torPreferences.requestBridgeDialogTitle "ブリッジをリクエスト">
<!ENTITY torPreferences.requestBridgeDialogWaitPrompt "BridgeDB に接続しています。しばらくお待ちください。">
<!ENTITY torPreferences.requestBridgeDialogSolvePrompt "ブリッジをリクエストするには CAPTCHA を解いて下さい。">
<!ENTITY torPreferences.requestBridgeErrorBadSolution "その解答は正しくありません。再度実行してください。">
<!-- Provide bridge dialog -->
<!ENTITY torPreferences.provideBridgeTitle "ブリッジを入力">
<!ENTITY torPreferences.provideBridgeHeader "信頼できる情報源から入手したブリッジの情報を入力してください。">
<!-- Connection settings dialog -->
<!ENTITY torPreferences.connectionSettingsDialogTitle "接続設定">
<!ENTITY torPreferences.connectionSettingsDialogHeader "Tor Browser がインターネットに接続する方法を設定します">
<!ENTITY torPreferences.firewallPortsPlaceholder "カンマ区切り">
<!-- Log dialog -->
<!ENTITY torPreferences.torLogsDialogTitle "Tor ログ">

<!-- #24746 about:torconnect strings -->
<!ENTITY torConnect.notConnectedConcise "接続していません">
<!ENTITY torConnect.connectingConcise "接続しています…">
<!ENTITY torConnect.tryingAgain "再試行…">
<!ENTITY torConnect.noInternet "Tor Browser はインターネットに接続できませんでした">
<!ENTITY torConnect.noInternetDescription "This could be due to a connection issue rather than Tor being blocked. Check your Internet connection, proxy and firewall settings before trying again.">
<!ENTITY torConnect.couldNotConnect "Tor Browser は Tor に接続できませんでした">
<!ENTITY torConnect.assistDescriptionConfigure "接続を設定"> <!-- used as a text to insert as a link on several strings (#1) -->
<!ENTITY torConnect.assistDescription "Tor があなたの現在地でブロックされている場合、ブリッジをお試しください。接続アシストで、現在地に適切なものを選ぶか、手動で#1できます。"> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.tryingBridge "ブリッジを試しています…">
<!ENTITY torConnect.tryingBridgeAgain "再試行中…">
<!ENTITY torConnect.errorLocation "Tor Browser はあなたの現在地を見つけられませんでした">
<!ENTITY torConnect.errorLocationDescription "Tor Browser が最適なブリッジを選択するには、あなたの現在地が必要です。現在地を共有したくない場合は、手動で#1してください。"> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.isLocationCorrect "この現在地の設定は正しいですか？">
<!ENTITY torConnect.isLocationCorrectDescription "Tor Browser は Tor に接続できませんでした。現在地の設定が正しいことを確認して再試行するか、#1してください。"> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.finalError "Tor Browser はまだ未接続です">
<!ENTITY torConnect.finalErrorDescription "Despite its best efforts, connection assist was not able to connect to Tor. Try troubleshooting your connection and adding a bridge manually instead.">
<!ENTITY torConnect.breadcrumbAssist "接続アシスト">
<!ENTITY torConnect.breadcrumbLocation "現在地の設定">
<!ENTITY torConnect.breadcrumbTryBridge "ブリッジを試す">
<!ENTITY torConnect.automatic "自動">
<!ENTITY torConnect.selectCountryRegion "国や地域を選択">
<!ENTITY torConnect.frequentLocations "よく選択される現在地">
<!ENTITY torConnect.otherLocations "その他の現在地">
<!ENTITY torConnect.restartTorBrowser "Tor Browser を再起動">
<!ENTITY torConnect.configureConnection "接続を構成…">
<!ENTITY torConnect.viewLog "ログを表示…">
<!ENTITY torConnect.tryAgain "もう一度やり直してください">
<!ENTITY torConnect.offline "インターネットに接続できません">
<!ENTITY torConnect.connectMessage "Tor 設定の変更は接続するまで反映されません">
<!ENTITY torConnect.tryAgainMessage "Tor Browser は、Tor ネットワークへの接続を確立できませんでした">
<!ENTITY torConnect.yourLocation "あなたの現在地">
<!ENTITY torConnect.tryBridge "ブリッジを試す">
<!ENTITY torConnect.autoBootstrappingFailed "自動設定に失敗しました">
<!ENTITY torConnect.autoBootstrappingFailed "自動設定に失敗しました">
<!ENTITY torConnect.cannotDetermineCountry "ユーザーの国を特定できませんでした">
<!ENTITY torConnect.noSettingsForCountry "現在地で使用可能な設定がありません">
