<!ENTITY torsettings.dialog.title "Tor 네트워크 설정">
<!ENTITY torsettings.wizard.title.default "Tor에 연결하기">
<!ENTITY torsettings.wizard.title.configure "Tor 네트워크 설정">
<!ENTITY torsettings.wizard.title.connecting "연결되는 중">

<!-- For locale picker: -->
<!ENTITY torlauncher.localePicker.title "Tor 브라우저 언어">
<!ENTITY torlauncher.localePicker.prompt "언어를 선택해 주십시오.">

<!-- For "first run" wizard: -->

<!ENTITY torSettings.connectPrompt "To에 연결하기 위해 &quot;연결&quot;을 클릭하세요.">
<!ENTITY torSettings.configurePrompt "Tor를 검열하는 국가(이집트, 중국, 터키 등) 나 프록시가 필요한 사설 네트워크에서 연결하는 경우, &quot;설정&quot;을 클릭하여 네트워크 설정을 조정합니다.">
<!ENTITY torSettings.configure "구성">
<!ENTITY torSettings.connect "연결">

<!-- Other: -->

<!ENTITY torsettings.startingTor "Tor 시작 대기 중...">
<!ENTITY torsettings.restartTor "Tor 재시작">
<!ENTITY torsettings.reconfigTor "재구성">

<!ENTITY torsettings.discardSettings.prompt "Tor 브리지를 구성했거나 로컬 프록시 설정을 입력했습니다.&#160;Tor 네트워크에 직접 연결하려면 이 설정을 제거해야 합니다.">
<!ENTITY torsettings.discardSettings.proceed "설정 제거 및 연결">

<!ENTITY torsettings.optional "옵션">

<!ENTITY torsettings.useProxy.checkbox "인터넷에 연결할 때 프록시를 사용합니다">
<!ENTITY torsettings.useProxy.type "프록시 종류">
<!ENTITY torsettings.useProxy.type.placeholder "프록시 유형을 선택해 주세요.">
<!ENTITY torsettings.useProxy.address "주소">
<!ENTITY torsettings.useProxy.address.placeholder "IP 주소 또는 호스트 네임">
<!ENTITY torsettings.useProxy.port "포트">
<!ENTITY torsettings.useProxy.username "Username">
<!ENTITY torsettings.useProxy.password "비밀번호">
<!ENTITY torsettings.useProxy.type.socks4 "SOCKS 4">
<!ENTITY torsettings.useProxy.type.socks5 "SOCKS 5">
<!ENTITY torsettings.useProxy.type.http "HTTP / HTTPS">
<!ENTITY torsettings.firewall.checkbox "이 컴퓨터는 특정 포트만 허용하는 방화벽을 통해서 연결됩니다.">
<!ENTITY torsettings.firewall.allowedPorts "허용된 포트">
<!ENTITY torsettings.useBridges.checkbox "우리나라에서는 Tor를 검열합니다.">
<!ENTITY torsettings.useBridges.default "제공된 브릿지를 선택하기">
<!ENTITY torsettings.useBridges.default.placeholder "브리지를 선택하세요.">
<!ENTITY torsettings.useBridges.bridgeDB "torproject.org에서 브릿지 요청하기">
<!ENTITY torsettings.useBridges.captchaSolution.placeholder "이미지에 나온 글자를 입력하세요.">
<!ENTITY torsettings.useBridges.reloadCaptcha.tooltip "새로운 캡차 시도값을 구합니다.">
<!ENTITY torsettings.useBridges.captchaSubmit "제출확인">
<!ENTITY torsettings.useBridges.custom "내가 아는 브리지를 제공">
<!ENTITY torsettings.useBridges.label "신뢰 가능한 출처에서 받은 브리지 정보를 입력하세요">
<!ENTITY torsettings.useBridges.placeholder "주소:포트번호 입력하세요.(한줄에 하나씩)">

<!ENTITY torsettings.copyLog "Tor log를 클립보드에 복사하기">

<!ENTITY torsettings.proxyHelpTitle "프록시 도움말">
<!ENTITY torsettings.proxyHelp1 "회사나 학교 혹은 대학 네트워크를 통해 연결하는 경우에 로컬 프록시가 필요할 수 있습니다.&#160;프록시가 필요한지 모르는 경우에는 다른 브라우저에서 인터넷 설정을 확인하거나 시스템의 네트워크 설정을 확인하세요.">

<!ENTITY torsettings.bridgeHelpTitle "브리지 중계서버 도움말">
<!ENTITY torsettings.bridgeHelp1 "브리지는 Tor 네트워크에 대한 연결을 차단하는 것을 더 어렵게 해주는 비공개 중계서버입니다.&#160; 각 유형의 브리지는 검열을 피하기 위해 각기 다른 방법을 사용합니다. &#160; obfs는 트래픽을 임의의 노이즈처럼 보이게 해줍니다. 그리고 meek은 Tor 대신에 meek 서비스에 연결되어있는 것처럼 보이게 해줍니다.">
<!ENTITY torsettings.bridgeHelp2 "일부 국가에서 어떤 방식으로든 Tor를 차단하려고 하기 때문에, 어떤 브리지는 특정 국가에서는 동작하지만 다른 곳에서는 그렇지 않습니다.&#160; 당신의 나라에서 어떤 브리지가 동작하고 있는지 모르는 경우 torproject.org/about/contact.html#support를 방문하세요.">

<!-- Progress -->
<!ENTITY torprogress.pleaseWait "Tor 네트워크에 연결할 때까지 기다려주십시오.&#160; 몇 분 정도 소요될 수 있습니다.">

<!-- #31286 about:preferences strings -->
<!ENTITY torPreferences.categoryTitle "Tor 연결">
<!ENTITY torPreferences.torSettings "Tor 설정">
<!ENTITY torPreferences.torSettingsDescription "Tor 브라우저는 전세계 수천명의 자원 봉사자에 의해 운영되는 Tor 네트워크와 당신을 연결합니다." >
<!ENTITY torPreferences.learnMore "더 알아보기">
<!-- Status -->
<!ENTITY torPreferences.statusInternetLabel "인터넷:">
<!ENTITY torPreferences.statusInternetTest "테스트">
<!ENTITY torPreferences.statusInternetOnline "온라인">
<!ENTITY torPreferences.statusInternetOffline "오프라인">
<!ENTITY torPreferences.statusTorLabel "Tor 네트워크:">
<!ENTITY torPreferences.statusTorConnected "접속됨">
<!ENTITY torPreferences.statusTorNotConnected "연결되지 않음">
<!ENTITY torPreferences.statusTorBlocked "차단됐을 가능성이 있습니다">
<!ENTITY torPreferences.learnMore "더 알아보기">
<!-- Quickstart -->
<!ENTITY torPreferences.quickstart "빠른 시작">
<!ENTITY torPreferences.quickstartDescriptionLong "'빠른시작'을 선택하면 Tor 브라우저가 실행될 때 Tor 네트워크에 자동으로 접속됩니다.  이때 설정 방식은 직전의 연결 설정을 따릅니다.">
<!ENTITY torPreferences.quickstartCheckbox "항상 자동으로 연결">
<!-- Bridge settings -->
<!ENTITY torPreferences.bridges "브리지">
<!ENTITY torPreferences.bridgesDescription "'브리지'는 Tor가 차단된 환경에서 Tor 네트워크에 접속하려 할 때 유용합니다. 주어진 환경에 따라 브리지를 사용하는 게 다른 중계서버를 사용하는 것 보다 원할할 수 있습니다.">
<!ENTITY torPreferences.bridgeLocation "귀하의 위치">
<!ENTITY torPreferences.bridgeLocationAutomatic "자동">
<!ENTITY torPreferences.bridgeLocationFrequent "자주 선택하는 위치">
<!ENTITY torPreferences.bridgeLocationOther "다른 위치">
<!ENTITY torPreferences.bridgeChooseForMe "Tor 브라우저에서 귀하에게 맞는 브리지를 자동으로 선택하도록 설정…">
<!ENTITY torPreferences.bridgeBadgeCurrent "귀하의 현재 브리지">
<!ENTITY torPreferences.bridgeBadgeCurrentDescription "추가한 브리지를 저장해두면, 귀하가 연결할 때 사용할 브리지 중 하나로 Tor가 고르게 될 겁니다. 필요한 때에 맞춰 Tor에서 자동으로 브리지를 바꾸는 작업을 진행합니다.">
<!ENTITY torPreferences.bridgeId "#1 브리지: #2"> <!-- #1 = bridge type; #2 = bridge emoji id -->
<!ENTITY torPreferences.remove "제거하기">
<!ENTITY torPreferences.bridgeDisableBuiltIn "내장 브리지를 비활성화">
<!ENTITY torPreferences.bridgeShare "QR 코드를 활용하거나, 다음 주소를 복사해 이 브리지를 공유할 수 있습니다:">
<!ENTITY torPreferences.bridgeCopy "브리지 주소 복사">
<!ENTITY torPreferences.copied "복사됨!">
<!ENTITY torPreferences.bridgeShowAll "모든 브리지 조회">
<!ENTITY torPreferences.bridgeRemoveAll "모든 브리지 삭제">
<!ENTITY torPreferences.bridgeAdd "새로운 브리지 추가">
<!ENTITY torPreferences.bridgeSelectBrowserBuiltin "Tor 브라우저 기본 내장 브리지 중에서 선택하기">
<!ENTITY torPreferences.bridgeSelectBuiltin "내장 브리지를 선택하기">
<!ENTITY torPreferences.bridgeRequest "브리지 요청하기">
<!ENTITY torPreferences.bridgeEnterKnown "기존에 알고 있던 브리지 주소를 입력하기">
<!ENTITY torPreferences.bridgeAddManually "브리지를 수동으로 추가하기…">
<!-- Advanced settings -->
<!ENTITY torPreferences.advanced "고급설정">
<!ENTITY torPreferences.advancedDescription "Tor 브라우저에서 인터넷에 연결하는 방식을 설정하세요">
<!ENTITY torPreferences.advancedButton "설정…">
<!ENTITY torPreferences.viewTorLogs "Tor 로그 조회">
<!ENTITY torPreferences.viewLogs "로그 조회">
<!-- Remove all bridges dialog -->
<!ENTITY torPreferences.removeBridgesQuestion "모든 브리지를 삭제할까요?">
<!ENTITY torPreferences.removeBridgesWarning "이 작업이 이루어진 후 되돌릴 수 없습니다.">
<!ENTITY torPreferences.cancel "취소">
<!-- Scan bridge QR dialog -->
<!ENTITY torPreferences.scanQrTitle "QR 코드 스캔">
<!-- Builtin bridges dialog -->
<!ENTITY torPreferences.builtinBridgeTitle "내장 브리지">
<!ENTITY torPreferences.builtinBridgeDescription "Tor 브라우저엔 '장착형 전송수단'이라는 특수 유형의 브리지가 있습니다.">
<!ENTITY torPreferences.builtinBridgeObfs4 "obfs4">
<!ENTITY torPreferences.builtinBridgeObfs4Description "obfs4는 내장 브리지의 일종입니다. Tor 트래픽이 랜덤하게 보이도록 만드는 역할을 담당합니다. obfs4는 obfs3 브리지와 같은 기존의 내장 브리지와 달리 차단 가능성도 적습니다.">
<!ENTITY torPreferences.builtinBridgeSnowflake "Snowflake">
<!ENTITY torPreferences.builtinBridgeSnowflakeDescription "Snowflake는 검열을 타파할 수 있는 내장 브리지입니다. 자원 봉사자 분들이 운영하는 Snowflake 프록시에 귀하의 연결이 거쳐 전송되도록 설정함으로써 이를 실현합니다. ">
<!ENTITY torPreferences.builtinBridgeMeekAzure "meek-azure">
<!ENTITY torPreferences.builtinBridgeMeekAzureDescription "meek-azure는 Tor를 사용하는 걸 감추고, Microsoft 웹 사이트를 이용중인 것처럼 만들어주는 내장 브리지입니다.">
<!-- Request bridges dialog -->
<!ENTITY torPreferences.requestBridgeDialogTitle "브리지 요청하기">
<!ENTITY torPreferences.requestBridgeDialogWaitPrompt "브리지DB에 연결하고 있으니, 잠시 기다려주세요.">
<!ENTITY torPreferences.requestBridgeDialogSolvePrompt "브리지를 요청하기 위해 CAPCHA를 풀어주세요.">
<!ENTITY torPreferences.requestBridgeErrorBadSolution "제출한 답이 맞지 않습니다. 다시 시도해주세요.">
<!-- Provide bridge dialog -->
<!ENTITY torPreferences.provideBridgeTitle "브리지 제공">
<!ENTITY torPreferences.provideBridgeHeader "신뢰 가능한 출처에서 받은 브리지 정보를 입력하세요">
<!-- Connection settings dialog -->
<!ENTITY torPreferences.connectionSettingsDialogTitle "연결 설정">
<!ENTITY torPreferences.connectionSettingsDialogHeader "Tor 브라우저에서 인터넷에 연결하는 방식을 설정하세요">
<!ENTITY torPreferences.firewallPortsPlaceholder "쉼표로 구분 된 값">
<!-- Log dialog -->
<!ENTITY torPreferences.torLogsDialogTitle "Tor 로그">

<!-- #24746 about:torconnect strings -->
<!ENTITY torConnect.notConnectedConcise "연결되지 않음">
<!ENTITY torConnect.connectingConcise "접속중...">
<!ENTITY torConnect.tryingAgain "다시 시도하는 중입니다…">
<!ENTITY torConnect.noInternet "Tor 브라우저에서 인터넷에 연결할 수 없습니다">
<!ENTITY torConnect.noInternetDescription "This could be due to a connection issue rather than Tor being blocked. Check your Internet connection, proxy and firewall settings before trying again.">
<!ENTITY torConnect.couldNotConnect "Tor 브라우저에서 Tor에 연결할 수 없습니다">
<!ENTITY torConnect.assistDescriptionConfigure "연결 세부 설정"> <!-- used as a text to insert as a link on several strings (#1) -->
<!ENTITY torConnect.assistDescription "귀하의 지역에서 Tor가 차단됐다면, 브리지를 사용하는 게 도움이 될 수 있습니다. '연결 도우미'로 귀하의 위치에 기반한 적합한 브리지를 제공받을 수 있습니다. 아니면 #1 수동으로 브리지를 선택할 수도 있습니다."> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.tryingBridge "브리지 사용…">
<!ENTITY torConnect.tryingBridgeAgain "한 번 더 해보고 있습니다…">
<!ENTITY torConnect.errorLocation "Tor 브라우저에서 귀하의 위치를 파악할 수 없습니다.">
<!ENTITY torConnect.errorLocationDescription "Tor 브라우저에서 귀하의 환경에 맞는 브리지를 선택할 수 있도록, 귀하의 위치 정보가 필요합니다. 귀하의 위치를 공유하는 게 꺼려진다면, 자동으로 하지 마시고 #1 수동으로 설정하세요."> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.isLocationCorrect "위치가 제대로 설정된 게 맞나요?">
<!ENTITY torConnect.isLocationCorrectDescription "Tor 브라우저에서 Tor에 여전히 연결할 수 없는 상태입니다. 귀하의 '위치 설정'이 올바르게 돼있는지 점검해본 후 다시 시도해주세요. 혹은 #1의 방식을 대신 써보세요."> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.finalError "Tor Browser still cannot connect">
<!ENTITY torConnect.finalErrorDescription "Despite its best efforts, connection assist was not able to connect to Tor. Try troubleshooting your connection and adding a bridge manually instead.">
<!ENTITY torConnect.breadcrumbAssist "연결 도우미">
<!ENTITY torConnect.breadcrumbLocation "위치 설정">
<!ENTITY torConnect.breadcrumbTryBridge "브리지 사용하기">
<!ENTITY torConnect.automatic "자동">
<!ENTITY torConnect.selectCountryRegion "국가 혹은 지역을 선택하세요">
<!ENTITY torConnect.frequentLocations "자주 선택하는 위치">
<!ENTITY torConnect.otherLocations "다른 위치">
<!ENTITY torConnect.restartTorBrowser "Tor 브라우저 재시작">
<!ENTITY torConnect.configureConnection "연결 구성중...">
<!ENTITY torConnect.viewLog "로그 조회…">
<!ENTITY torConnect.tryAgain "다시 시도하기">
<!ENTITY torConnect.offline "인터넷에 연결할 수 없습니다">
<!ENTITY torConnect.connectMessage "Tor 설정의 변경사항은 Tor에 연결하기 전까지 적용되지 않습니다">
<!ENTITY torConnect.tryAgainMessage "Tor 브라우저에서 Tor 네트워크에 연결하는 데 실패했습니다">
<!ENTITY torConnect.yourLocation "귀하의 위치">
<!ENTITY torConnect.tryBridge "브리지 사용하기">
<!ENTITY torConnect.autoBootstrappingFailed "자동 구성 실패">
<!ENTITY torConnect.autoBootstrappingFailed "자동 구성 실패">
<!ENTITY torConnect.cannotDetermineCountry "사용자의 국가를 파악할 수 없습니다">
<!ENTITY torConnect.noSettingsForCountry "귀하의 위치에서 이용할 수 있는 설정이 없습니다">
